"""
Fitting models to data summarized by pairs of loci, incorporating both allele frequencies and linkage disequilibrium
"""
from . import numerics, integration, demographics, inference, plotting, TLSpectrum_mod
TLSpectrum = TLSpectrum_mod.TLSpectrum