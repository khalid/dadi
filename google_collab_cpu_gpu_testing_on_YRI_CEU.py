# -*- coding: utf-8 -*-
"""
Under google Colaboratory create a new project

First, you'll need to enable GPUs for the notebook:

    Navigate to Edit→Notebook Settings
    select GPU from the Hardware Accelerator drop-down
"""
# Install requirments
!pip install matplotlib
!pip install numpy scipy
!pip install pycuda scikit-cuda

#!pip install dadi

# We will install dadi from a modified source hosted in mbb
# MyInference.py : modif. du fichier Infrenec.py de dadi pour incorporer les optimisations anneal et dual_anneal
# local_dual_anneal.py : modif du fichier scipy change to LocalSearchWrapper to allow the use of bounded minimizers other than L-BFGS-B
#                        or L-BFGS-B with a maxiter < than the default LS_MAXITER_MIN 

!git clone https://gitlab.mbb.univ-montp2.fr/khalid/dadi.git
import os
os.chdir('dadi')
!python setup.py install  

#L'insallation ne semble pas se faire correctement, il faut ajouter ceci
import sys
sys.path.append('/usr/local/lib/python3.6/dist-packages/dadi-2.1.0-py3.6-linux-x86_64.egg')

import warnings
warnings.filterwarnings('ignore')




# Use YRI_CEU example
os.chdir('examples/YRI_CEU')
from numpy import array
import timeit
import dadi
# In demographic_models.py, we've defined a custom model for this problem
import demographic_models

# Load the data
data = dadi.Spectrum.from_file('YRI_CEU.fs')
ns = data.sample_sizes


# The Demographics1D and Demographics2D modules contain a few simple models,
# mostly as examples. We could use one of those.
func = dadi.Demographics2D.split_mig
# Instead, we'll work with our custom model
func = demographic_models.prior_onegrow_mig

# Now let's optimize parameters for this model.

# The upper_bound and lower_bound lists are for use in optimization.
# Occasionally the optimizer will try wacky parameter values. We in particular
# want to exclude values with very long times, very small population sizes, or
# very high migration rates, as they will take a long time to evaluate.
# Parameters are: (nu1F, nu2B, nu2F, m, Tp, T)
upper_bound = [100, 100, 100, 10, 3, 3]
lower_bound = [1e-2, 1e-2, 1e-2, 0.001, 0.001, 0.001]

# This is our initial guess for the parameters, which is somewhat arbitrary.
p0 = [2,0.1,2,1,0.2,0.2]
# Make the extrapolating version of our demographic model function.
func_ex = dadi.Numerics.make_extrap_log_func(func)

# Perturb our parameters before optimization. This does so by taking each
# parameter a up to a factor of two up or down.
p0 = dadi.Misc.perturb_params(p0, fold=1, upper_bound=upper_bound,
                              lower_bound=lower_bound)
# Do the optimization. By default we assume that theta is a free parameter,
# since it's trivial to find given the other parameters. If you want to fix
# theta, add a multinom=False to the call.
# The maxiter argument restricts how long the optimizer will run. For real 
# runs, you will want to set this value higher (at least 10), to encourage
# better convergence. You will also want to run optimization several times
# using multiple sets of intial parameters, to be confident you've actually
# found the true maximum likelihood parameters.
print('Beginning optimization ************************************************')


# These are the grid point settings will use for extrapolation.
pts_l = [140,150,160]

########  CPU optimize_log ######################
def optimize_log_cpu(pts_l):
    dadi.cuda_enabled(False)
    popt = dadi.Inference.optimize_log(p0, data, func_ex, pts_l, 
                                       lower_bound=lower_bound,
                                       upper_bound=upper_bound,
                                       verbose=0, maxiter=3)

########  GPU optimize_log ######################
def optimize_log_gpu(pts_l):
    dadi.cuda_enabled(True)
    popt = dadi.Inference.optimize_log(p0, data, func_ex, pts_l, 
                                       lower_bound=lower_bound,
                                       upper_bound=upper_bound,
                                       verbose=0, maxiter=3)

print('CPU (s): pts_l =' + str(pts_l))

cpu_time = timeit.timeit('optimize_log_cpu(pts_l)', number=5, setup="from __main__ import optimize_log_cpu, pts_l")
print(cpu_time)
print('GPU (s): pts_l =' + str(pts_l))
gpu_time = timeit.timeit('optimize_log_gpu(pts_l)', number=5, setup="from __main__ import optimize_log_gpu, pts_l")
print(gpu_time)
print('GPU speedup over CPU: {:.2f}x'.format(cpu_time/gpu_time)+ ' pts_l =' + str(pts_l))


########  Benchmark optimize_dual_anneal ######################

maxiterGlobal=10 #maximum global search iterations - in each iteration, it explores twice the number of parameters
accept=1 #parameter for acceptance distribution (lower values makes the probability of acceptance smaller)
visit=1.01 #parameter for visiting distribution (higher values makes the algorithm jump to a more distant region)
Tini=50 #initial temperature
no_local_search=False #If set to True, a Generalized Simulated Annealing will be performed with no local search strategy applied
local_method='L-BFGS-B' #local search method
maxiterLocal=20 #maximum local search iterations
verbose=False


def dual_anneal_cpu(pts_l):
    dadi.cuda_enabled(False)
    popt = dadi.MyInference.optimize_dual_anneal(p0=p0, data=data, model_func=func_ex, pts=pts_l,
                                                     lower_bound=lower_bound, upper_bound=upper_bound,
                                                     no_local_search=no_local_search, local_method=local_method, local_maxiter=maxiterLocal,
                                                     maxiter=maxiterGlobal, Tini=Tini, accept=accept, visit=visit, verbose=verbose, full_output=True)   

def dual_anneal_gpu(pts_l):
    dadi.cuda_enabled(True)
    popt = dadi.MyInference.optimize_dual_anneal(p0=p0, data=data, model_func=func_ex, pts=pts_l,
                                                     lower_bound=lower_bound, upper_bound=upper_bound,
                                                     no_local_search=no_local_search, local_method=local_method, local_maxiter=maxiterLocal,
                                                     maxiter=maxiterGlobal, Tini=Tini, accept=accept, visit=visit, verbose=verbose, full_output=True)   

print('CPU (s): pts_l =' + str(pts_l))
cpu_time = timeit.timeit('dual_anneal_cpu(pts_l)', number=5, setup="from __main__ import dual_anneal_cpu, pts_l")
print(cpu_time)
print('GPU (s): pts_l =' + str(pts_l))
gpu_time = timeit.timeit('dual_anneal_gpu(pts_l)', number=5, setup="from __main__ import dual_anneal_gpu, pts_l")
print(gpu_time)
print('GPU speedup over CPU: {:.2f}x'.format(cpu_time/gpu_time)+ ' pts_l =' + str(pts_l))